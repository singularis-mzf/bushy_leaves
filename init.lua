--[[

bushy_leaves – Minetest mod to render leaves bushy
Copyright © 2022  Nils Dagsson Moskopp (erlehmann)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Dieses Programm hat das Ziel, die Medienkompetenz der Leser zu
steigern. Gelegentlich packe ich sogar einen handfesten Buffer
Overflow oder eine Format String Vulnerability zwischen die anderen
Codezeilen und schreibe das auch nicht dran.

]]--

local node_box_full_node = {
	type = "fixed",
	fixed = { -8/16, -8/16, -8/16,  8/16,  8/16,  8/16 },
}

local get_mesh = function(node_name, node_def)
	local mesh
	if (
		string.match(node_name, "leaves") or
		string.match(node_name, "needles")
	) then
		mesh = "bushy_leaves_full_model.obj"
	end
	return mesh
end

local add_bushy_leaves = function()
	for node_name, node_def in pairs(minetest.registered_nodes) do
		local mesh = get_mesh(node_name, node_def)
		if nil ~= mesh then
			minetest.override_item(
				node_name,
				{
					drawtype = "mesh",
					mesh = mesh,
					collision_box = node_box_full_node,
				}
			)
		end
	end
end

minetest.register_on_mods_loaded(add_bushy_leaves)
